DPM Cluster via docker
==================
Configuration and setup of a DPM cluster via docker compose

docker-compose up -d

Containers
==========

The following containers will be part of the cluster on a dedicated network

* DPM Head  (head.dpmcluster.cern.ch)
* DPM Disk  (disk01.dpmcluster.cern.ch)
* DPM DB    (db.dpmcluster.cern.ch)
* DPM Client (client.dpmcluster.cern.ch)

DPM Head and disk containers run the following services

* Xrootd server
* LCGDM Dav server
* Gridftp server


Certificates
===========

The certificates for Head and Disk are coming from a custom CA and are available in the certs folder

Configuration
=============

The dteam VO is configured on the cluster at the following namespace path /dpm/cern.ch/home/dteam

The cluster has a preconfigured pool with a Filesytem  on the Disknode and a quotatoken of 5GB
