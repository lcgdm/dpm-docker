mkdir /etc/grid-security/dpmmgr

chmod 400 /etc/grid-security/hostkey.pem
cp /etc/grid-security/hostcert.pem  /etc/grid-security/dpmmgr/dpmcert.pem
cp /etc/grid-security/hostkey.pem   /etc/grid-security/dpmmgr/dpmkey.pem

chown -R dpmmgr:dpmmgr /etc/dmlite*
chown -R dpmmgr:dpmmgr /etc/grid-security/dpmmgr
chown -R dpmmgr:dpmmgr /etc/xrootd/dpmxrd-sharedkey.dat
chmod 400 /etc/xrootd/dpmxrd-sharedkey.dat

fetch-crl

cp -r /root/certs/* /etc/grid-security/certificates

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf &

sleep 30

dmlite-shell -e "mkdir /dpm/cern.ch/home/dteam p"
dmlite-shell -e "acl  /dpm/ d:u::rwx,d:g::rwx,d:o::r-x,u::rwx,g::r-x,o::r-x set"
dmlite-shell -e "acl /dpm/cern.ch/  d:u::rwx,d:g::rwx,d:o::r-x,u::rwx,g::r-x,o::r-x set"

dmlite-shell -e "groupadd dteam"
dmlite-shell -e "chgrp  /dpm/cern.ch/home/dteam   dteam"
dmlite-shell -e "chown /dpm/cern.ch/home/dteam root"
dmlite-shell -e "chmod /dpm/cern.ch/home/dteam 775"

dmlite-shell -e "acl /dpm/cern.ch/home  d:u::rwx,d:g::rwx,d:o::r-x,u::rwx,g::rwx,o::r-x set"
dmlite-shell -e "acl /dpm/cern.ch/home/dteam  d:u::rwx,d:g::rwx,d:o::r-x,u::rwx,g::rwx,o::r-x set"


dmlite-shell -e "pooladd pool01 filesystem P"
dmlite-shell -e "fsadd /data  pool01 disk01.dpmcluster.cern.ch"

dmlite-shell -e "quotatokenset  /dpm/cern.ch/home/dteam pool pool01 size 5GB desc test_token groups dteam"

sleep 100000000000000000000000
