mkdir /etc/grid-security/dpmmgr
cp /etc/grid-security/hostcert.pem  /etc/grid-security/dpmmgr/dpmcert.pem
cp /etc/grid-security/hostkey.pem   /etc/grid-security/dpmmgr/dpmkey.pem
chown -R dpmmgr:dpmmgr /etc/dmlite*
chown -R dpmmgr:dpmmgr /etc/grid-security/dpmmgr
chown -R dpmmgr:dpmmgr /etc/xrootd/dpmxrd-sharedkey.dat

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf 
