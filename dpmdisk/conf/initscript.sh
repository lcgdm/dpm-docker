mkdir /etc/grid-security/dpmmgr
chmod 400 /etc/grid-security/hostkey.pem
cp /etc/grid-security/hostcert.pem  /etc/grid-security/dpmmgr/dpmcert.pem
cp /etc/grid-security/hostkey.pem   /etc/grid-security/dpmmgr/dpmkey.pem
chown -R dpmmgr:dpmmgr /etc/dmlite*
chown -R dpmmgr:dpmmgr /etc/grid-security/dpmmgr
chown -R dpmmgr:dpmmgr /etc/xrootd/dpmxrd-sharedkey.dat
chmod 400  /etc/xrootd/dpmxrd-sharedkey.dat

mkdir /data
chown dpmmgr:dpmmgr /data

fetch-crl

cp  -r /root/certs/* /etc/grid-security/certificates

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf 
