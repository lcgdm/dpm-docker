DPM Dome  docker images
==================
This is a first attempt to build a docker image for DPM, in particular the one with Dome Flavour ( without the Legacy stack)

Content 
======

The images provide CentOS7 containers for :

* DPM Head
* DPM Disk
* DPM DB

DPM Head  and disk runs the following services

* Xrootd server
* LCGDM Dav server
* Gridftp server


Certificates
===========

DPM needs an X509 certificate to work, therefore it has to be provided on the hosting node with this layout

- /etc/grid-security/hostcert.pem
- /etc/grid-security/hostkey.pem
- /etc/grid-security/dpmmgr/dpmcert.pem
- /etc/grid-security/dpmmgr/dpmkey.pem

the CA certificates need also be installed under /etc/grid-security/certificates ( as the default igtf installation)

the folder is mounted on the container then as a volume

Configuration
=============

The file under the userconf folder have to be customized with DB connection details and hostnames

Running via docker composer
--------------------------
docker-compose up -d
